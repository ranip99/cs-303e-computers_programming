class Link(object):

    def __init__ (self, data, next = None):
        self.data = data
        self.next = next

class CircularList(object):
    
    # Constructor
    def __init__ ( self ):
        self.first = Link(None)
        self.first.next = self.first

    # Insert an element (value) in the list
    def insert ( self, data):

        new = Link(data)
        if self.first.data == None:
            self.first = new
            self.first.next = new
            return
        current = self.first
        while current.next != self.first:
            current = current.next
        current.next = new
        new.next = self.first
        

    # Find the link with the given data (value)
    def find ( self, data ):

        data = Link(data)
        current = self.first
        while data.data != current.data:
            current = current.next
            if current == self.first:
                return None
        return current

    # Delete a link with a given data (value)
    def delete ( self, data ):
        
        previous = self.first
        while previous.next != self.first:
            previous = previous.next
        current = self.first

        if (current == None):
          return None

        while (current.data != data):
          if (current.next == self.first):
            return None
          previous = current
          current = current.next

        if (current == self.first):
          self.first = self.first.next
          previous.next = self.first
          return data 
        else:
          previous.next = current.next

        return current.data

    # Delete the nth link starting from the Link start 
    # Return the next link from the deleted Link
    def delete_after ( self, start, n ):
        
        current = self.find(start)
        count = 1
        if current == None:
            return None
        while n != count:
            current = current.next
            count += 1
        num = current.data
        self.delete(current.data)
        return num, current.next.data
        
    # Return a string representation of a Circular List
    def __str__ ( self ):
        
        current = self.first
        while current.next != self.first:
            print(current.data, end = "  ")
            current = current.next
        print(current.data)
        return ""
        




def main(num_soldiers, starting_soldier, elim_num):
    #return a list with the order in which the soldiers were removed in
    soldiers = CircularList()
    for i in range(1, num_soldiers + 1):
        soldiers.insert(i)

    dead = []
    start = starting_soldier
    for i in range(num_soldiers - 1):
        num, start = soldiers.delete_after(start, elim_num)
        dead.append(num)

    return dead
