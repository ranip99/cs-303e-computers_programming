#  File: GuessingGame.py
#  Description: Guess the correct number in 10 tries.
#  Student's Name: Rani Patel
#  Student's UT EID: rjp2524
#  Course Name: CS 303E 
#  Unique Number: 50191
#
#  Date Created: 02/26/2020
#  Date Last Modified: 02/26/2020


def main():

    #Welcomes Guessser and asks for a number
    print("Welcome to the guessing game. You have ten tries to guess my number.")
    guess = int(input("Please enter your guess: "))

    #Sets starting try as 1 and the correct number as 1458
    tries = 1
    number = 1458

    #Loop allows ten tries only
    while tries < 11:
        
        #Makes sure guess is in correct range
        if guess >= 1 and guess <= 9999:
            
            #Correct first try prints separate output
            if guess == number and tries == 1:
                print("That's correct!")
                print("Congratulations! You guessed it on the first try!")
                #Leaves loop since guessed correctly in less than 10 tries
                break
            
            #Outputs correct for any other correct try other than first
            elif guess == number:
                print("That's correct!")
                print("Congratulations! You guessed it in",tries,"guesses.")
                break
            
            #Notifies guesser their guess is too low
            elif guess < number:
                print("Your guess is too low.")
                print("Guesses so far:", tries)
                
                #On the last try they get no more guesses and exits loop
                if tries == 10:
                    print("Game over: you ran out of guesses.")
                    break
                #Continues guessing if they have more tries
                tries += 1
                guess = int(input("Please enter your guess: "))
                
            #Notifies guesser their number is too high
            elif guess > number:
                print("Your guess is too high.")
                print("Guesses so far:", tries)
                if tries == 10:
                    print("Game over: you ran out of guesses.")
                    break
                tries += 1
                guess = int(input("Please enter your guess: "))
                
        #Guess is not within range so ask for a new guess on same try
        else:
            print("Your guess must be between 0001 and 9999.")
            guess = int(input("Please enter a valid guess: "))
    
main()
