# Input: a rectangle which is a tuple of 4 integers (x1, y1, x2, y2)
# Output: an integer giving the area of the rectangle
def area (rect):

    side1 = rect[2] - rect[0]
    side2 = rect[3] - rect[1]
    return side1 * side2

# Input: two rectangles in the form of tuples of 4 integers
# Output: a tuple of 4 integers denoting the overlapping rectanlge.
#         return (0, 0, 0, 0) if there is no overlap
def overlap (rect1, rect2):

    if ((rect2[0] < rect1[0] < rect2[2] or rect2[0] < rect1[2] < rect2[2]) \
        or (rect1[0] < rect2[0] < rect1[2] or rect1[0] < rect2[2] < rect1[2])) \
       and ((rect2[1] < rect1[1] < rect2[3] or rect2[1] < rect1[3] < rect2[3]) \
            or (rect1[1] < rect2[1] < rect1[3] or rect1[1] < rect2[3] < rect1[3])):
        
        if rect2[0] < rect1[0] < rect2[2]:
            if rect2[0] < rect1[2] < rect2[2]:
                x1 = rect1[0]
                x2 = rect1[2]
            else:
                x1 = rect1[0]
                x2 = rect2[2]
                
        elif rect2[0] < rect1[2] < rect2[2]:
            x1 = rect2[0]
            x2 = rect1[2]

        elif rect1[0] < rect2[0] < rect1[2]:
            if rect1[0] < rect2[2] <= rect1[2]:
                x1 = rect2[0]
                x2 = rect2[2]
            else:
                x1 = rect1[0]
                x2 = rect2[2]

        elif rect1[0] < rect2[2] < rect1[2]:
            x1 = rect1[0]
            x2 = rect2[2] 
        
        if rect2[1] < rect1[1] < rect2[3]:
            if rect2[1] < rect1[3] < rect2[3]:
                y1 = rect1[1]
                y2 = rect1[3]
                
            else:
                y1 = rect1[1]
                y2 = rect2[3]
            
        elif rect1[1] < rect2[1] < rect1[3]:
            if rect1[1] < rect2[3] < rect1[3]:
                y1 = rect2[1]
                y2 = rect2[3]
            else:
                y1 = rect2[1]
                y2 = rect1[3]

        elif rect2[1] < rect1[3] < rect2[3]:
            y1 = rect2[1]
            y2 = rect1[3]
                
        elif rect1[1] < rect2[3] < rect1[3]:
            y1 = rect1[1]
            y2 = rect2[3]
            
            
    elif ((rect1[0] == rect2[0] and rect1[2] == rect2[2]) or \
         (rect1[1] == rect2[1] and rect1[3] == rect2[3])) \
         and ((rect2[0] < rect1[0] < rect2[2] or rect1[0] < rect2[0] < rect1[2] \
               or rect2[0] < rect1[2] < rect2[2] or rect1[0] < rect2[2] < rect1[2]) \
              or (rect2[1] < rect1[1] < rect2[3] or rect1[1] < rect2[1] < rect1[3] \
               or rect2[1] < rect1[3] < rect2[3] or rect1[1] < rect2[3] < rect1[3])):
        
        if rect1[0] == rect2[0] and rect1[2] == rect2[2]:
            x1 = rect1[0]
            x2 = rect1[2]
            if rect1[1] < rect2[1] < rect1[3]:
                y1 = rect2[1]
                y2 = rect1[3]
            elif rect2[1] < rect1[1] < rect2[3]:
                y1 = rect1[1]
                y2 = rect2[3]
            elif rect1[1] < rect2[3] < rect1[3]:
                y1 = rect2[3]
                y2 = rect1[3]
            elif rect2[1] < rect1[3] < rect2[3]:
                y1 = rect1[3]
                y2 = rect2[3]
            
        elif rect1[1] == rect2[1] and rect1[3] == rect2[3]:
            y1 = rect1[1]
            y2 = rect1[3]
            if rect1[0] < rect2[0] < rect1[2]:
                x1 = rect2[0]
                x2 = rect1[2]
            elif rect2[0] < rect1[0] < rect2[2]:
                x1 = rect1[0]
                x2 = rect2[2]
            elif rect1[0] < rect2[2] < rect1[2]:
                x1 = rect2[2]
                x2 = rect1[2]
            elif rect2[0] < rect1[2] < rect2[2]:
                x1 = rect1[2]
                x2 = rect2[2]
                
        elif (rect1[0] == rect2[0] or rect1[2] == rect2[2]) \
             and (rect1[1] == rect2[1] or rect1[3] == rect2[3]):
            
            if rect1[0] == rect2[0]:
                x1 = rect1[0]
                if rect1[2] < rect2[2]:
                    x2 = rect1[2]
                else:
                    x2 = rect2[2]
            
            if rect1[2] == rect2[2]:
                x2 = rect1[2]
                if rect1[0] < rect2[0]:
                    x1 = rect2[0]
                else:
                    x1 = rect1[0]

            if rect1[1] == rect2[1]:
                y1 = rect1[1]
                if rect1[3] < rect2[3]:
                    y2 = rect1[3]
                else:
                    y2 = rect2[3]
            
            if rect1[3] == rect2[3]:
                y2 = rect1[3]
                if rect1[1] < rect2[1]:
                    y1 = rect2[1]
                else:
                    y1 = rect1[1]
        
            
    else:
        return (0,0,0,0)

    return (x1, y1, x2, y2)

# Input: bldg is a rectangle in the form of a tuple of 4 integers
#        representing the whole office space
#        cubicles is a list of tuples of 4 integers representing
#        all the requested cubicles
# Output: a single integer denoting the area of the unallocated 
#         space in the office
def unallocated_space (bldg, cubicles):

    overlapArea = 0
    space = 0
    count = 0
    extra = []
    overlaps = []
    for i in range(len(cubicles)):
        for j in range(len(cubicles)):
            if i != j:
                overlapRect = overlap(cubicles[i], cubicles[j])
                overlapArea += area(overlapRect)
                if overlapRect != (0,0,0,0):
                    if overlapRect not in overlaps:
                        overlaps.append(overlapRect)
        space += area(cubicles[i])
    
    for i in range(len(overlaps)):
        for j in range(i+1,len(overlaps)):
            
            if i == j:
                continue 
            else:
                if overlap(overlaps[j], overlaps[i]) != (0,0,0,0):
                    extra.append(overlap(overlaps[j], overlaps[i]))

    for i in range(len(extra)):
        for j in range(i+1,len(extra)):
            if i == j:
                continue
            else:
                if extra[j] == extra[i]:
                    count += 1


    unallocated = area(bldg) - space + overlapArea // 2 - count 
    return unallocated

# Input: bldg is a rectangle in the form of a tuple of 4 integers
#        representing the whole office space
#        cubicles is a list of tuples of 4 integers representing
#        all the requested cubicles
# Output: a single integer denoting the area of the contested 
#         space in the office
def contested_space (bldg, cubicles):
    
    contested = 0
    overlaps = []
    extra = []
    count = 0

    for i in range(len(cubicles)):
        for j in range(len(cubicles)):
            if i != j:
                overlapRect = overlap(cubicles[i], cubicles[j])
                contested += area(overlapRect)
                if overlapRect != (0,0,0,0):
                    overlaps.append(overlapRect)
    
    for i in range(len(overlaps)):
        for j in range(len(overlaps)):
            pass
        if i == j:
            continue 
        else:
            if overlap(overlaps[j], overlaps[i]) != (0,0,0,0):
                extra.append(overlap(overlaps[j], overlaps[i]))
    
    for i in range(len(extra)):
        for j in range(len(extra)):
            if i == j:
                continue
            else:
                if extra[j] == extra[i]:
                    count += 1
 
    return contested // 2 - count // 2


# Input: rect is a rectangle in the form of a tuple of 4 integers
#        representing the cubicle requested by an employee
#        cubicles is a list of tuples of 4 integers representing
#        all the requested cubicles
# Output: a single integer denoting the area of the uncontested 
#         space in the office that the employee gets
def uncontested_space (rect, cubicles):

    overlapArea = 0
    space = area(rect)
    overlaps = []
    count = 0
    extra = []

    for i in range(len(cubicles)):
        overlapRect = overlap(rect, cubicles[i])
        if overlapRect != (0,0,0,0):
            overlaps.append(overlapRect)
    
    for i in range(len(overlaps)):
        for j in range(i,len(overlaps)):
            if i == j:
                continue 
            else:
                if overlap(overlaps[j], overlaps[i]) != (0,0,0,0):
                    extra.append(overlap(overlaps[j], overlaps[i]))

    for i in range(len(extra)):
        for j in range(i,len(extra)):
            if extra[j] == extra[i]:
                count += 1
    
    for i in range(len(overlaps)):
        overlapArea += area(overlaps[i])
    
    uncontested = space - overlapArea + count

    return uncontested

def main(inpt, fptr):

    inpt = inpt.split()
    width, length = int(inpt[0]), int(inpt[1])
    bldg = (0,0,width, length)
    employees = int(inpt[2])
    cubicles = []
    names = []
    for i in range(3, employees * 5, 5):
        name = inpt[i]
        rect = (int(inpt[i+1]), int(inpt[i+2]), int(inpt[i+3]), int(inpt[i+4]))
        cubicles.append(rect)
        names.append(name)
    
    #Output Total Area
    fptr.write("Total: ")
    fptr.write(str(area(bldg)))
    fptr.write("\n")
    
    #Output Unallocated Area
    fptr.write("Unallocated: ")
    fptr.write(str(unallocated_space(bldg, cubicles)))
    fptr.write("\n")
    
    #Output Contested Area
    fptr.write("Contested: ")
    fptr.write(str(contested_space(bldg, cubicles)))
    fptr.write("\n")
    
    #Output each employees' guaranteed area
    for i in range(employees):
        fptr.write(names[i] + ": ")
        fptr.write(str(uncontested_space(cubicles[i], cubicles)))
        fptr.write("\n")


if __name__ == "__main__":
  main()
