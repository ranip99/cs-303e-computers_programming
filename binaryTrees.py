
class Node (object):
    def __init__ (self, data):

        self.data = data
        self.lchild = None
        self.rchild = None

class Tree (object):
    def __init__ (self):
        self.root = None

  # insert data into the tree
    def insert (self, data):
        new_node = Node (data)

        if (self.root == None):
            self.root = new_node
            return
        else:
            current = self.root
            parent = self.root
            
            while (current != None):
                parent = current
                if (data < current.data):
                    current = current.lchild
                else:
                    current = current.rchild

            # found location now insert node
            if (data < parent.data):
                parent.lchild = new_node
            else:
                parent.rchild = new_node
                
    # Returns true if two binary trees are similar
    def is_similar (self, aNode, pNode):

        if pNode is None and aNode is None:
            return True
        if pNode is None or aNode is None:
            return False
        else:
            return aNode.data == pNode.data and self.is_similar(aNode.lchild, pNode.lchild) and self.is_similar(aNode.rchild, pNode.rchild)


    # Prints out all nodes at the given level
    def print_level (self, level, aNode):
        
        if aNode is None:
            return
        if level == 1:
            print(aNode.data, end = " ")
        else:
            self.print_level(level - 1, aNode.lchild)
            self.print_level(level - 1, aNode.rchild)

    # Returns the height of the tree
    def get_height (self, aNode): 
        
        if aNode is None:
            return 0
        else:
            return max(self.get_height(aNode.lchild), self.get_height(aNode.rchild)) + 1

    # Returns the number of nodes in tree which is 
    # equivalent to 1 + number of nodes in the left 
    # subtree + number of nodes in the right subtree
    def num_nodes (self, aNode):

        if aNode is None:
            return 0
        else:
            return 1 + self.num_nodes(aNode.lchild) + self.num_nodes(aNode.rchild)


def main():

    data1 = input().split()
    data2 = input().split()
    tree1 = Tree()
    tree2 = Tree()

    for num in data1:
        tree1.insert(eval(num))

    for num in data2:
        tree2.insert(eval(num))

    print("The Trees are similar:", tree1.is_similar(tree1.root, tree2.root))
    print()

    print("Levels of Tree 1:")
    for i in range (1, tree1.get_height(tree1.root) + 1):
        tree1.print_level(i,tree1.root)
        print()
    print()

    print("Levels of Tree 2:")
    for i in range (1, tree2.get_height(tree2.root) + 1):
        tree2.print_level(i,tree2.root)
        print()
    print()

    print("Height of Tree 1:", tree1.get_height(tree1.root) - 1)
    print("Nodes in Tree 1:", tree1.num_nodes(tree1.root))

    print("Height of Tree 2:", tree2.get_height(tree2.root) - 1)
    print("Nodes in Tree 2:", tree2.num_nodes(tree2.root))

main()
