#  Input: dim is a positive odd integer
#  Output: function returns a 2-D list of integers arranged
#          in a spiral

from math import ceil

def create_spiral (dim):
  
    spiral = []
    for row in range(dim):
        r = [] 
        for col in range(dim):
          r.append(0) 
        spiral.append(r)

    row = dim // 2 
    col = dim // 2
    count = 1
    number = 1
    
    #turning the list into a spiral 

    while number != dim ** 2 + 1:
        for i in range(ceil(count/2)):
            
            spiral[row][col] = number

            #upward counters: 4, 8, 12...
            if count % 4 == 0:
                row -= 1
            #down
            elif count % 4 == 2:
                row += 1
            #right
            elif count % 4 == 1:
                col += 1
            #left
            elif count % 4 == 3:
                col -= 1
                
            number += 1
            
        count += 1
    print_arr(spiral)
        
    return spiral


#temp is a 2D array/list
def print_arr (temp):
    mx = max((len(str(ele)) for sub in temp for ele in sub))
    for row in temp:
        print(" ".join(["{:<{mx}}".format(ele,mx=mx) for ele in row]))
        

 
#  Input: grid a 2-D list containing a spiral of numbers
#         val is a number within the range of numbers in
#         the grid
#  Output: sum_sub_grid returns the sum of the numbers (including val)
#          surrounding the parameter val in the grid 
#          if val is out of bounds, returns -1
def sum_sub_grid (grid, val):
    # a 2-D list containing a spiral of numbers

    #finding position of val
    for x in range(len(grid)):
        for y in range(len(grid)):
            if val == grid[x][y]:
                r = x
                c = y
                break
      

    #if val is not found return -1
    for i in range(len(grid)):
	    found = val not in grid[i]
    if found == True:
      return -1
      
    #find surrounding numbers
    #top_left
    if grid[r-1][c-1] #is not found:
      top_left = 0
    else:
      top_left = grid[r-1][c-1]
  ##################################################################
    #i cant think of how to make the top statement to work but the
    #bottom might work?

    #top left
    if r-1 > dim-1 or c-1 > dim-1:
      top_left = 0
    else:
      top_left = grid[r-1][c-1]
    

    
    top_right = grid[r-1][c+1]
    top = grid[r-1][c]
    bottom = grid[r+1][c]
    bot_left = grid[r+1][c-1]
    bot_right = grid[r+1][c+1]
    left = grid[r][c-1]
    right = grid[r][c+1]


    total = top_left + top_right + top + bottom + bot_left + bot_right + val + left + right

    return total


def main():
    # read the dimension of the grid and value from input file
    inFile = open("Spiral.in.txt", "r") 
    data = inFile.readline().split()
    dim = int(data[0])
    findNumber = int(data[1])
    outFile = open("Spiral.out.txt", "w") 

    # test that dimension is odd

    if dim % 2 == 0:
        dim += 1   

    # create a 2-D list representing the spiral
    grid = create_spiral(dim)
  
  
    # find sum of adjacent terms
    total = sum_sub_grid(grid, findNumber)

    # write the sum in the output file
    outFile.write(str(total))
    outFile.close()
    inFile.close()

#if __name__ == "__main__": 
    
main()
