#  File: Grades.py
#  Description: Writes a file with a gradebook from a inputted file
#  Student's Name: Rani Patel
#  Student's UT EID: rjp2524
#  Course Name: CS 303E 
#  Unique Number: 50191
#
#  Date Created: 4/26/2020
#  Date Last Modified: 4/30/2020

def main():

    #open the input and output file
    grades = open("gradeInput.txt", "r")
    file = open("gradeOutput.txt", "w")

    #format the header of the gradebook
    file.write(format("","15s") + format("","15s") + format("HW  ",">7s") +
               format("Exam",">7s") + format("Final",">8s") + "\n")
    file.write(format("Last Name","15s") + format("First Name","15s") +
               format("Avg ",">7s") + format("Avg ",">7s") + format("Grade",">8s")+"\n")

    file.write("-" * 52 + "\n")

    #sets line as the first line of the input
    line = grades.readline().split()

    #loop that goes through each line (person)
    while line != []:

        #sets each number from a str to int
        line[1:19] = [int(line[i]) for i in range(1,19)]

        #splits first and last name
        name = line[0].split(",")

        #outputs name formatted
        file.write(format(name[0], "15s") + format(name[1], "15s"))

        #adds homework grades and averages them
        hmwk = sum(line[1:16])
        avgH = hmwk / 15
        file.write(format(avgH, "7.1f"))

        #outputs exam average
        exam = sum(line[16:19])
        avgE = exam / 3
        file.write(format(avgE, "7.1f"))

        #calculates final grade and outputs it
        final = 0.45 * avgE + 0.55 * avgH
        file.write(format(final, "7.1f") + "\n")

        #read the next line
        line = grades.readline().split()

    file.close()
    grades.close()    
 
main()
