#  File: TestLinkedList.py

#  Description: Different methods of using Linked Lists

#  Student Name: Rani Patel

#  Student UT EID: rjp2524

#  Course Name: CS 313E

#  Unique Number: 84825

#  Date Created: 7/20/2020

#  Date Last Modified: 7/26/2020

class Link (object):
  
  def __init__ (self, data, next = None):
    self.data = data
    self.next = next

class LinkedList (object):
  
  # initialize the linked list
  def __init__ (self):
    self.first = None

  # get number of links 
  def get_num_links (self):
    
    current = self.first
    count = 1
    while current.next != None:
      count += 1
      current = current.next
    return count

    
  # add an item at the beginning of the list
  def insert_first (self, data):
    
    new_link = Link (data)
    new_link.next = self.first
    self.first = new_link


  # add an item at the end of a list
  def insert_last (self, data):
    
    new_link = Link (data)
    current = self.first
    if (current == None):
      self.first = new_link
      return
    while (current.next != None):
      current = current.next
    current.next = new_link


  # add an item in an ordered list in ascending order
  def insert_in_order (self, data):
    
    new = Link(data)
    current = self.first
    while current != None:
      if new.data < current.next.data:
        temp = current.next
        current.next = new
        new.next = temp
        return
      current = current.next
    

  # search in an unordered list, return None if not found
  def find_unordered (self, data):
    
    current = self.first
    if (current == None):
      return None

    while (current.data != data):
      if (current.next == None):
        return None
      else:
        current = current.next

    return current.data


  # Search in an ordered list, return None if not found
  def find_ordered (self, data):
    
    current = self.first
    if (current == None):
      return None

    while current.data <= data:
      if current.data == data:
        return current.data
      if current.next == None:
        return None
      else:
        current = current.next
    return current.data


  # Delete and return Link from an unordered list or None if not found
  def delete_link (self, data):
    
    previous = self.first
    current = self.first

    if (current == None):
      return None

    while (current.data != data):
      if (current.next == None):
        return None
      else:
        previous = current
        current = current.next

    if (current == self.first):
      self.first = self.first.next
    else:
      previous.next = current.next

    return current.data


  # String representation of data 10 items to a line, 2 spaces between data
  def __str__ (self):
    
    current = self.first
    count = 0
    while current:
      if count % 10 == 0:
        print()
      print(current.data, end = "  ")
      count += 1
      current = current.next
  
    return ""

    
  # Copy the contents of a list and return new list
  def copy_list (self):
    
    new = LinkedList()
    current = self.first
    while current != None:
      new.insert_last(current.data)
      current = current.next
    return new


  # Reverse the contents of a list and return new list
  def reverse_list (self):
    
    new = LinkedList()
    current = self.first
    while current != None:
      new.insert_first(current.data)
      current = current.next
    return new


  # Sort the contents of a list in ascending order and return new list
  def sort_list (self):
    
    end = None
    while end != self.first:
      current = self.first
      while current.next != end:
        nxt = current.next
        if current.data > nxt.data:
          current.data, nxt.data = nxt.data, current.data
        current = current.next
      end = current
    return

  # Return True if a list is sorted in ascending order or False otherwise
  def is_sorted (self):
    
    current = self.first
    while current.next != None:
      if current.data > current.next.data:
        return False
      current = current.next
    return True


  # Return True if a list is empty or False otherwise
  def is_empty (self):
    
    if self.first == None:
      return True
    return False
  

  # Merge two sorted lists and return new list in ascending order
  def merge_list (self, other):
    
    new = LinkedList()
    current1 = self.first
    current2 = other.first
    idx1 = 0
    idx2 = 0
    len1 = self.get_num_links()
    len2 = other.get_num_links()

    while current1 and current2:
      if current1.data <= current2.data:
        new.insert_last(current1.data)
        current1 = current1.next
        idx1 += 1
      elif current1.data > current2.data:
        new.insert_last(current2.data)
        current2 = current2.next
        idx2 += 1

    if idx1 < len1:
      while current1 != None:
        new.insert_last(current1.data)
        idx1 += 1
        current1 = current1.next

    if idx2 < len2:
      while current2 != None:
        new.insert_last(current2.data)
        idx1 += 2
        current2 = current2.next

    return new


  # Test if two lists are equal, item by item and return True
  def is_equal (self, other):
    
    if self.get_num_links() != other.get_num_links():
      return False
    first = self.first
    sec = other.first
    while first != None:
      if first != sec:
        return False
      first = first.next
      sec = sec.next
    return True


  # Return a new list, keeping only the first occurence of an element
  # and removing all duplicates. Do not change the order of the elements.
  def remove_duplicates (self):

    new = self.copy_list()
    current = new.first
    second = new.first
    while current != None:
      while second.next != None:
        if current.data == second.next.data:   
          second.next = second.next.next   
        else:
          second = second.next
      second = current.next
      current = current.next
      
    return new


def main():
  
  # Test methods insert_first() and __str__() by adding more than
  # 10 items to a list and printing it.
  test = LinkedList()
  test.insert_first(1)
  test.insert_first(2)
  test.insert_first(3)
  test.insert_first(4)
  test.insert_first(5)
  test.insert_first(6)
  test.insert_first(7)
  test.insert_first(8)
  test.insert_first(9)
  test.insert_first(12)
  test.insert_first(15)
  test.insert_first(17)
  test.insert_first(20)
  test.insert_first(24)
  test.insert_first(35)

  # Test method insert_last()
  test2 = LinkedList()
  test2.insert_last(16)
  test2.insert_last(2)
  test2.insert_last(34)
  test2.insert_last(74)
  test2.insert_last(52)
  test2.insert_last(86)
  test2.insert_last(7)
  test2.insert_last(28)
  test2.insert_last(93)
  test2.insert_last(14)
  test2.insert_last(86)
  test2.insert_last(12)
  test2.insert_last(55)
  test2.insert_last(24)
  
  # Test method insert_in_order()
  test2.insert_in_order(7)

  # Test method get_num_links()
  test2.get_num_links()

  # Test method find_unordered() 
  # Consider two cases - data is there, data is not there
  test.find_unordered(3)
  test.find_unordered(19)
  
  # Test method find_ordered() 
  # Consider two cases - data is there, data is not there
  test2.find_ordered(3)
  test2.find_ordered(19)

  # Test method delete_link()
  # Consider two cases - data is there, data is not there
  test.delete_link(15)
  test.delete_link(0)

  # Test method copy_list()
  test.copy_list()

  # Test method reverse_list()
  test.reverse_list()

  # Test method sort_list()
  test.sort_list()
  test2.sort_list()

  # Test method is_sorted()
  # Consider two cases - list is sorted, list is not sorted
  test2.is_sorted()
  test.is_sorted()

  # Test method is_empty()
  test3 = LinkedList()
  test3.is_empty()
  test2.is_empty()

  # Test method merge_list()
  test.merge_list(test2)

  # Test method is_equal()
  # Consider two cases - lists are equal, lists are not equal
  test.is_equal(test2)
  

  # Test remove_duplicates()
  test2.remove_duplicates()

if __name__ == "__main__":
  main()
