class Stack (object):
    def __init__ (self):
        self.stack = []

    # add an item to the top of the stack
    def push (self, item):
        self.stack.append ( item )

    # remove an item from the top of the stack
    def pop (self):
        return self.stack.pop()

    # check what item is on top of the stack without removing it
    def peek (self):
        return self.stack[len(self.stack) - 1]

    # check if a stack is empty
    def isEmpty (self):
        return (len(self.stack) == 0)

    # return the number of elements in the stack
    def size (self):
        return (len(self.stack))

class Node (object):
    def __init__ (self, data):
        self.data = data
        self.lChild = None
        self.rChild = None


class Tree (object):
    def __init__ (self):
        self.root = Node(None)

    def create_tree (self, expr):
        current = self.root
        st = Stack()
        for token in expr:
            if token == "(":
                current.lChild = Node(token)
                st.push(current)
                current = current.lChild
            elif token == '+' or token == '-' or token == '*' or token == '/' \
                 or token == '//' or token == '%' or token == '**':
                current.data = token
                print(current.data)
                st.push(current)
                current.rChild = Node(token)
                current = current.rChild
            elif token == ")":
                if st.isEmpty() == False:
                    current = st.pop()
            elif token.split() == []:
                continue
            else:
                current.data = eval(token)
                if st.isEmpty() == False:
                    current = st.pop()
                

    def evaluate (self, aNode):
        if aNode is None:
            return 
        if aNode.lChild == None and aNode.rChild == None:
            return aNode.data        
        if aNode != None:
        
            l = self.evaluate(aNode.lChild)
            r = self.evaluate(aNode.rChild)
            if aNode.data == "+":
                return l + r
            elif aNode.data == "-":
                return l - r
            elif aNode.data == "**":
                return l ** r
            elif aNode.data == "//":
                return l // r
            elif aNode.data == "/":
                return l / r
            elif aNode.data == "%":
                return l % r
            elif aNode.data == "*":
                return l * r


    def pre_order(self, aNode):
        lt = []
        if (aNode != None):
            lt.append(aNode.data)
            lt = lt + self.pre_order(aNode.lChild)
            lt = lt + self.pre_order(aNode.rChild)
        return lt

    def post_order(self, aNode):
        lt = []
        if (aNode != None):
            lt = lt + self.post_order(aNode.lChild)
            lt = lt + self.post_order(aNode.rChild)
            lt.append(aNode.data)
        return lt


def main():
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    expr = input()
    e = []
    st = ""
    for i in expr:
        if i == " ":
            e.append(st)
            st = ""
        else:
            st = st + i

    tokens = Tree()
    tokens.create_tree(e)

    fptr.write(expr)
    fptr.write(" = ")
    fptr.write(str(float(tokens.evaluate(tokens.root))))
    fptr.write("\n\n")
    fptr.write("Prefix Expression: ")

    pre = tokens.pre_order(tokens.root)
    for x in pre:
        fptr.write(str(x))
        fptr.write(" ")

    fptr.write("\n\n")

    post = tokens.post_order(tokens.root)
    fptr.write("Postfix Expression: ")
    for x in post:
        fptr.write(str(x))
        fptr.write(" ")
    fptr.write("\n")

main()
