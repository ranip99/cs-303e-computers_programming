#* Code for Graph
class Stack (object):
  def __init__ (self):
    self.stack = []

  # add an item to the top of the stack
  def push (self, item):
    self.stack.append (item)

  # remove an item from the top of the stack
  def pop (self):
    return self.stack.pop()

  # check the item on the top of the stack
  def peek (self):
    return self.stack[-1]

  # check if the stack if empty
  def is_empty (self):
    return (len (self.stack) == 0)

  # return the number of elements in the stack
  def size (self):
    return (len (self.stack))


class Queue (object):
  def __init__ (self):
    self.queue = []

  # add an item to the end of the queue
  def enqueue (self, item):
    self.queue.append (item)

  # remove an item from the beginning of the queue
  def dequeue (self):
    return (self.queue.pop(0))

  # check if the queue is empty
  def is_empty (self):
    return (len (self.queue) == 0)

  # return the size of the queue
  def size (self):
    return (len (self.queue))

  #check the first item of the queue
  def peek(self):
    return self.queue[0]


class Vertex (object):
  def __init__ (self, label):
    self.label = label
    self.visited = False

  # determine if a vertex was visited
  def was_visited (self):
    return self.visited

  # determine the label of the vertex
  def get_label (self):
    return self.label

  # string representation of the vertex
  def __str__ (self):
    return str (self.label)


class Graph (object):
  def __init__ (self):
    self.Vertices = []
    self.adjMat = []

  # check if a vertex is already in the graph
  def has_vertex (self, label):
    nVert = len (self.Vertices)
    for i in range (nVert):
      if (label == (self.Vertices[i]).get_label()):
        return True
    return False

  # given the label get the index of a vertex
  def get_index (self, label):
    nVert = len(self.Vertices)
    for i in range (nVert):
      if (label == (self.Vertices[i]).get_label()):
        return i
    return -1

  # add a Vertex with a given label to the graph
  def add_vertex (self, label):
    if (not self.has_vertex (label)):
      self.Vertices.append (Vertex (label))

    # add a new column in the adjacency matrix
    nVert = len (self.Vertices)
    for i in range (nVert - 1):
      (self.adjMat[i]).append (0)

    # add a new row for the new vertex
    new_row = []
    for i in range (nVert):
      new_row.append (0)
    self.adjMat.append (new_row)

  # add weighted directed edge to graph
  def add_directed_edge (self, start, finish, weight = 1):
    self.adjMat[start][finish] = weight

  # add weighted undirected edge to graph
  def add_undirected_edge (self, start, finish, weight = 1):
    self.adjMat[start][finish] = weight
    self.adjMat[finish][start] = weight

  # return an unvisited vertex adjacent to vertex v (index)
  def get_adj_unvisited_vertex (self, v):
    nVert = len (self.Vertices)
    for i in range (nVert):
      if (self.adjMat[v][i] > 0) and (not (self.Vertices[i]).was_visited()):
        return i
    return -1

  # get a copy of the list of Vertex objects
  def get_vertices (self):
    for i in range(len(self.Vertices)):
      print(self.Vertices[i])

  # do a depth first search in a graph
  def dfs (self, v):

    # the stack is empty, let us rest the flags
    #nVert = len (self.Vertices)
    #for i in range (nVert):
    #  (self.Vertices[i]).visited = False
      
    # create the Stack
    theStack = Stack ()

    # mark the vertex v as visited and push it on the Stack
    (self.Vertices[v]).visited = True
    print (self.Vertices[v])
    theStack.push (v)

    # visit all the other vertices according to depth
    while (not theStack.is_empty()):
      # get an adjacent unvisited vertex
      u = self.get_adj_unvisited_vertex (theStack.peek())
      if (u == -1):
        u = theStack.pop()
      else:
        (self.Vertices[u]).visited = True
        print (self.Vertices[u])
        theStack.push (u)


    print(self.Vertices[v] == True)

  # do the breadth first search in a graph
  def bfs (self, v):
    #create a queue
    line = Queue ()

    # mark the vertex v as visited and insert it on the Queue
    (self.Vertices[v]).visited = True
    print (self.Vertices[v])
    line.enqueue (v)

    # visit all the other vertices according to depth
    while (not line.is_empty()):
      # get an adjacent unvisited vertex
      u = self.get_adj_unvisited_vertex (line.peek())
      if (u == -1):
        u = line.dequeue()
      else:
        (self.Vertices[u]).visited = True
        print (self.Vertices[u])
        line.enqueue (u)

    # the queue is empty, let us reset the flags
    nVert = len (self.Vertices)
    for i in range (nVert):
      (self.Vertices[i]).visited = False

    
  # delete an edge from the adjacency matrix
  # delete a single edge if the graph is directed
  # delete two edges if the graph is undirected
  def delete_edge (self, fromVertexLabel, toVertexLabel):
    fromIdx = self.get_index(fromVertexLabel)
    toIdx = self.get_index(toVertexLabel)
    one = self.adjMat[fromIdx][toIdx]
    self.adjMat[fromIdx][toIdx] = 0
    two = self.adjMat[toIdx][fromIdx]
    #if one == two:
    self.adjMat[toIdx][fromIdx] = 0
    

  # delete a vertex from the vertex list and all edges from and
  # to it in the adjacency matrix
  def delete_vertex (self, vertexLabel):
    nVert = len(self.Vertices)
    idx = self.get_index(vertexLabel)
    self.Vertices.remove(self.Vertices[idx])
    self.adjMat.pop(idx)
    for j in range(nVert - 1):
        del self.adjMat[j][idx]
  
def main():
    #try to use only print() statements, if not uncomment the line below
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')

    numVert = int(input())
    cities = Graph()
    for i in range(numVert):
        c = (input()).strip()
        cities.add_vertex(c)

    numEdges = int(input())
    for i in range(numEdges):
        e = (input()).strip().split()
        if len(e) == 2:
            cities.add_directed_edge(int(e[0]), int(e[1]))
        else:
            cities.add_directed_edge(int(e[0]), int(e[1]), int(e[2]))

    # test depth first search
    print("Depth First Search")
    fsCity = input()
    cities.dfs(cities.get_index(fsCity))
    print()

    # test breadth first search
    print("Breadth First Search")
    cities.bfs(cities.get_index(fsCity))
    print()

    # test deletion of an edge
    delete = input().split()
    one = delete[0]
    two = delete[1]
    cities.delete_edge(one, two)
    print("Deletion of an edge\n")
    print ("Adjacency Matrix")
    for i in range (numVert):
      for j in range (numVert):
        print (cities.adjMat[i][j], end = " ")
      print ()
    print ()

    # test deletion of a vertex
    ver = input()
    print("Deletion of a vertex\n")
    cities.delete_vertex(ver)
    print("List of Vertices")
    cities.get_vertices()
    print()

    print ("Adjacency Matrix")
    for i in range (numVert - 1):
      for j in range (numVert - 1):
        print (cities.adjMat[i][j], end = " ")
      print ()

main()
