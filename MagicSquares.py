#  File: MagicSquares.py
#  Description: Create magic squares x by x
#  Student's Name: Rani Patel
#  Student's UT EID: rjp2524
#  Course Name: CS 303E 
#  Unique Number: 50191
#
#  Date Created: 4/21/2020
#  Date Last Modified: 4/23/2020

import copy

class MagicSquare:

    def __init__(self, side):
        self.sideLength = side

        # make a side x side length grid with loops
        grid = []
        square = []
        for i in range(self.sideLength):
            square.append(0)
        for i in range(self.sideLength):
            grid.append(copy.deepcopy(square))

        #the column starts as the middle of the first row
        col = self.sideLength // 2
        row = 0

        #loop sets i from 1 to square of side
        for i in range(1,self.sideLength ** 2 + 1):
            
            grid[row][col] = i

            #go down instead of northeast for i factor of side
            if i % self.sideLength == 0:
                row += 1
                col = col
            #restart column when it is at end
            elif col == self.sideLength - 1:
                col = 0
                row -= 1
            #restart row
            elif row == -self.sideLength:
                row = -1
                col += 1
            #northeast
            else:
                row -= 1
                col += 1
        
        self.grid = grid

    def display(self):

        #format prints a side x side square in width of 5
        for i in range(self.sideLength):
            for a in range(self.sideLength):
                print(format(self.grid[i][a], "5d"),end = "")
            print()
        print()
            

def main():

    #prints all odd side length magic squares (1-13)
    for i in range(1,14,2):
        print("Magic Square of size", i)
        print()
        MagicSquare(i).display()

main()
        







        
        
    
