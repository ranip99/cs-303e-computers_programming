import math

class Point (object):
  # constructor
  def __init__(self, x = 0, y = 0):
    self.x = x
    self.y = y

  # get the distance to another Point object
  def dist (self, other):
    return math.hypot (self.x - other.x, self.y - other.y)

  # string representation of a Point
  def __str__ (self):
    return '(' + str(self.x) + ', ' + str(self.y) + ')'

  # equality tests of two Points
  def __eq__ (self, other):
    tol = 1.0e-8
    return ((abs(self.x - other.x) < tol) and (abs(self.y - other.y) < tol))

  def __ne__ (self, other):
    tol = 1.0e-8
    return ((abs(self.x - other.x) >= tol) or (abs(self.y - other.y) >= tol))

  def __lt__ (self, other):
    tol = 1.0e-8
    if (abs(self.x - other.x) < tol):
      if (abs(self.y - other.y) < tol):
        return False
      else:
        return (self.y < other.y)
    return (self.x < other.x)

  def __le__ (self, other):
    tol = 1.0e-8
    if (abs(self.x - other.x) < tol):
      if (abs(self.y - other.y) < tol):
        return True
      else:
        return (self.y <= other.y)
    return (self.x <= other.x)

  def __gt__ (self, other):
    tol = 1.0e-8
    if (abs(self.x - other.x) < tol):
      if (abs(self.y - other.y) < tol):
        return False
      else:
        return (self.y > other.y)
    return (self.x > other.x)

  def __ge__ (self, other):
    tol = 1.0e-8
    if (abs(self.x - other.x) < tol):
      if (abs(self.y - other.y) < tol):
        return True
      else:
        return (self.y >= other.y)
    return (self.x >= other.x)

# Input: p, q, r are Point objects
# Output: compute the determinant and return the value
def det (p, q, r):
  
  det = p.x * q.y + q.x * r.y + r.x * p.y - q.x * p.y - r.x * q.y - p.x * r.y
  return det

# Input: sorted_points is a sorted list of Point objects
# Output: computes the convex hull of a sorted list of Point objects
#         convex hull is a list of Point objects starting at the
#         extreme left point and going clockwise in order
#         returns the convex hull
def convex_hull (sorted_points):
  
  n = len(sorted_points)
  upper_hull = []
  upper_hull.append(sorted_points[0])
  upper_hull.append(sorted_points[1])
  
  for i in range(2,n):
    upper_hull.append(sorted_points[i])
    while len(upper_hull) >= 3 and det(upper_hull[-3],upper_hull[-2],upper_hull[-1]) >  0:
      upper_hull.pop(-2)

  lower_hull = []
  lower_hull.append(sorted_points[-1])
  lower_hull.append(sorted_points[-2])
  
  for i in range(n-3,-1,-1):
    lower_hull.append(sorted_points[i])
    while len(lower_hull) >= 3 and det(lower_hull[-3],lower_hull[-2],lower_hull[-1]) > 0:
      lower_hull.pop(-2)
    
  lower_hull.pop(0)
  lower_hull.pop(-1)

  for i in range(len(lower_hull)):
    upper_hull.append(lower_hull[i])
    
  convex_hull = upper_hull
  
  return convex_hull

# Input: convex_poly is  a list of Point objects that define the
#        vertices of a convex polygon in order
# Output: computes and returns the area of a convex polygon
def area_poly (convex_poly):
  
  det = 0
  z = len(convex_poly)
  
  for i in range(z-1):
    det += convex_poly[i].x * convex_poly[i+1].y
  det += convex_poly[z-1].x * convex_poly[0].y
  
  for i in range(z-1):
    det -= convex_poly[i].y * convex_poly[i+1].x
  det -= convex_poly[z-1].y * convex_poly[0].x
  
  area = (1/2) * abs (det)

  return area

# Input: no input
# Output: a string denoting all test cases have passed
def test_cases():
  # write your own test cases

  return "all test cases passed"

def main():
  # create an empty list of Point objects
  pts = []

  # open file hull.in for reading
  inFile = open("hull.in.txt","r")

  # read file line by line, create Point objects and store in a list
  data = int(inFile.readline())
  for i in range(data):
    temp = inFile.readline().split()
    pts.append(Point(int(temp[0]), int(temp[1])))
    
  # sort the list according to x-coordinates
  pts.sort()

  # get the convex hull
  convex = convex_hull(pts)

  # run your test cases

  # open file hull.out for writing

  # print the convex hull
  for i in range(len(convex)):
    print("(",convex[i].x, ",", convex[i].y, ")")

  # get the area of the convex hull
  hullArea = area_poly(convex)
  print(hullArea)

  # print the area of the convex hull

if __name__ == "__main__":
  main()
