# Input: tuples_list is an unsorted list of tuples denoting intervals
# Output: a list of merged tuples sorted by the lower number of the
#         interval
def merge_tuples (tuples_list):

  mergedList = []

  for i in range(len(tuples_list)):
    tempMin = tuples_list[i][0]
    tempMax = tuples_list[i][1]
    
    for j in range(len(tuples_list)):
      inner = tuples_list[j][0]
      outer = tuples_list[j][1]

      for x in range(len(tuples_list)):
        inner = tuples_list[x][0]
        outer = tuples_list[x][1]
        #inside
        if tempMin >= inner and tempMax <= outer:
          tempMin = inner
          tempMax = outer

        #outside  
        elif tempMin <= inner and tempMax >= outer:
          tempMin = tempMin
          tempMax = tempMax

        #rightward
        elif tempMin >= inner and tempMax >= outer and tempMin <= outer:
          tempMin = inner
          tempMax = tempMax

        #leftward
        elif tempMin <= inner and tempMax <= outer and tempMax >= inner:
          tempMin = tempMin
          tempMax = outer



    if (tempMin, tempMax) in mergedList:
      continue
    
    else:
      mergedList.append((tempMin,tempMax))
 
  mergedList.sort()
  
  return mergedList
          
 

# Input: tuples_list is a list of tuples of denoting intervals
# Output: a list of tuples sorted by ascending order of the size of
#         the interval
#         if two intervals have the size then it will sort by the
#         lower number in the interval
def sort_by_interval_size (tuples_list):

  for i in range(len(tuples_list)):
    space = abs(tuples_list[i][1]- tuples_list[i][0])
    sIndex = i

    for j in range(i+1,len(tuples_list)):
      if tuples_list[j][1] - tuples_list[j][0] < space:
        space = abs(tuples_list[j][1] - tuples_list[j][0])
        sIndex = j

    if sIndex != i:
      tuples_list[i],tuples_list[sIndex] = tuples_list[sIndex], tuples_list[i]
 

  return tuples_list


# Input: no input
# Output: a string denoting all test cases have passed
def test_cases ():
  assert merge_tuples([(1,2)]) == [(1,2)]
  # write your own test cases

  assert sort_by_interval_size([(1,3), (4,5)]) == [(4,5), (1,3)]
  # write your own test cases

  return "all test cases passed"

def main():
  # open file intervals.in and read the data and create a list of tuples
  inFile = open("intervals.in.txt", "r")
  n  = int(inFile.readline())
  nums = []
  for i in range(n):
    temp = inFile.readline().split()
    tuples = (int(temp[0]),int(temp[1]))
    nums.append(tuples)

  # merge the list of tuples
  mergedList = merge_tuples(nums)
  

  # sort the list of tuples according to the size of the interval
  sortedList = sort_by_interval_size(mergedList)
  print(sortedList)

  # run your test cases
  '''
  print (test_cases())
  '''

  # open file intervals.out and write the output list of tuples
  # from the two functions

if __name__ == "__main__":
  main()
