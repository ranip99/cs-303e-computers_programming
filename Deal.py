#  File: Deal.py
#  Description: Proving mathematically that switching is better
#  Student's Name: Rani Patel
#  Student's UT EID: rjp2524
#  Course Name: CS 303E 
#  Unique Number: 50191
#
#  Date Created: 03/28/2020
#  Date Last Modified: 03/30/2020

import  random

#this function produces a random number between 1 and n
def roll(n):

    #calls the library random
    number = random.randint(1,n)
    return number


#function generating numbers, comaparing them, and printing them
def runOneTrial():

    #produces a random number for prize, guess, view, and newGuess
    prize = roll(3)
    guess = roll(3)
    view = roll(3)
    newGuess = roll(3)

    #makes sure view is not the same number as prize and guess
    while view == prize or view == guess:
        view = roll(3)

    #makes sure newGuess is not the same as view and guess
    while newGuess == view or newGuess == guess:
        newGuess = roll(3)

    #prints each number formatted
    print(format(prize, "^5d"), format(guess, "^7d"), \
              format(view, "^6d"), format(newGuess, "^10d"))

    #returns win for those that win by switching to newGuess
    if newGuess == prize:
        return "win"

     
#main function
def main():

    #asking user for number or trials
    print()
    trials = int(input("How many trials do you want to run? "))
    print()

    #prints the titles for each column formatted
    print(format("Prize", "7s") + format("Guess", "7s") + \
          format("View", "6s") + format("New Guess", "7s"))

    #set initial count of wins
    count = 0
    
    #another variable with trials value because the loop makes it 0
    loopCount = trials
    
    #loop that calls on runOneTrial() until the number of trials=0
    while loopCount != 0:
        wins = runOneTrial()
        #keeps count of how many wins by switching
        if wins == "win":
            count += 1
        loopCount -= 1

    #calculates the winning chances of switching
    winnings = count / trials

    #prints the probabilities
    print()
    print("Probability of winning if you switch =", format(winnings,"4.2f"))
    print("Probability of winning if you do not switch =", format(1 - winnings,"4.2f"))
    print()
    
main()
