#  File: LinearEquations.py
#  Description: Return linear equation with many different functions
#  Student's Name: Rani Patel
#  Student's UT EID: rjp2524
#  Course Name: CS 303E 
#  Unique Number: 50191
#
#  Date Created: 04/06/2020
#  Date Last Modified:04/07/2020

############################################################################

class LinearEquation():

    #initializer
    def __init__(self, m, b):
        self.slope = m
        self.intercept = b
    #returns the y = mx + b equation
    def showEq(self):
        #conditional for if both slope and intercept = 0
        if self.slope != 0 or self.intercept != 0:

            #determines if slope or intercept is negative and adjusts sign
            if self.slope > 0:
                slope = str(self.slope) + "x "
                
            elif self.slope < 0:
                slope = "- " + str(abs(self.slope)) + "x "
                
            else:
                slope = ""

            if self.intercept > 0 and self.slope != 0:
                intercept = "+ " + str(self.intercept)
                
            elif self.intercept > 0 and self.slope == 0:
                intercept = str(self.intercept)
                
            elif self.intercept < 0:
                intercept = "- " + str(abs(self.intercept))

            else:
                intercept = ""

            #joins the mx with the b part of the equation 
            equation = slope + intercept
                
        else:
            equation = "0"
        
        return equation

    #adds two functions together
    def add(self, otherEq):
        m = self.slope + otherEq.slope
        b = self.intercept + otherEq.intercept
        return LinearEquation(m, b)

    #takes the difference of two functions
    def sub(self, otherEq):
        m = self.slope - otherEq.slope
        b = self.intercept - otherEq.intercept
        return LinearEquation(m, b)

    #solves for equations like f(g(x))
    def compose(self, otherEq):
        m = self.slope * otherEq.slope
        b = self.slope * otherEq.intercept + self.intercept
        return LinearEquation(m, b)

    #evaluates the function for the given value of x
    def evaluate(self, x):
        value = self.slope * x + self.intercept
        return value
        
#main program with given code
def main():
    
    f = LinearEquation(5,3)
    print("f(x) =",f.showEq())
    print("f(3) =",f.evaluate(3),"\n")
         
    g = LinearEquation(-2,-6)
    print("g(x) =",g.showEq())
    print("g(-2) =",g.evaluate(-2),"\n")

    h = f.add(g)
    print("h(x) = f(x) + g(x) =",h.showEq())
    print("h(-4) =",h.evaluate(-4),"\n")

    j = f.sub(g)
    print("j(x) = f(x) - g(x) =",j.showEq())
    print("j(-4) =",j.evaluate(-4),"\n")

    k = f.compose(g)
    print("f(g(x)) =",k.showEq(),"\n")

    m = g.compose(f)
    print("g(f(x)) =",m.showEq(),"\n")

    g = LinearEquation(5,-3)
    print("g(x) =",g.showEq())
    print("g(-2) =",g.evaluate(-2),"\n")

    h = f.add(g)
    print("h(x) = f(x) + g(x) =",h.showEq())
    print("h(-4) =",h.evaluate(-4),"\n")

    j = f.sub(g)
    print("j(x) = f(x) - g(x) =",j.showEq())
    print("j(-4) =",j.evaluate(-4),"\n")

main()





        
