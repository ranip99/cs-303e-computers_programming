
import time

# Input: v an integer representing the minimum lines of code and
#        k an integer representing the productivity factor
# Output: computes the sum of the series (v + v // k + v // k**2 + ...)
#         returns the sum of the series
def sum_series (v, k):

  line = v
  total = v
  x = 1
  while line > 0:
    line = v // k ** x
    total += line
    x += 1
  return total
    
# Input: n an integer representing the total number of lines of code
#        k an integer representing the productivity factor
# Output: returns v the minimum lines of code to write using linear search
def linear_search (n, k):

  v = n
  for i in range(n):
    if sum_series(v,k) < n:
      return v + 1
    else:
      v -= 1 
 

# Input: n an integer representing the total number of lines of code
#        k an integer representing the productivity factor
# Output: returns v the minimum lines of code to write using binary search
def binary_search (n, k):

    lo = 0
    hi = n - 1
    temp = sum_series(n, k)
    while (lo <= hi):
        mid = (lo + hi) // 2
        if (sum_series(mid, k) < n):
            lo = mid + 1
        elif (sum_series(mid, k) >= n):
            hi = mid - 1
            if temp > mid:
                temp = mid
    return temp 

# Input: no input
# Output: a string denoting all test cases have passed
def test_cases():
  # write your own test cases

  return "all test cases passed"

def main():
  in_file = open("work.in.txt", "r")
  num_cases = int((in_file.readline()).strip())

  for i in range(num_cases):
    inp = (in_file.readline()).split()
    n = int(inp[0])
    k = int(inp[1])

    start = time.time()
    print("Binary Search: " + str(binary_search(n, k)))
    finish = time.time()
    print("Time: " + str(finish - start))

    print()

    start = time.time()
    print("Linear Search: " + str(linear_search(n, k)))
    finish = time.time()
    print("Time: " + str(finish - start))

    print()
    print()

if __name__ == "__main__":
  main()
