import math

class Point (object):
  # constructor with default values
  def __init__ (self, x = 0, y = 0, z = 0):
    
      self.x = x
      self.y = y
      self.z = z

  # create a string representation of a Point
  # returns a string of the form (x, y, z)
  def __str__ (self):
      
      return ("(" + str(self.x) + ", " + str(self.y) + ", " + str(self.z) + ")")

  # get distance to another Point object
  # other is a Point object
  # returns the distance as a floating point number
  def distance (self, other):

      dist = ((self.x - other.x) ** 2 + (self.y - other.y) ** 2 +
              (self.z - other.z) ** 2) ** (1/2)
      
      return dist

  # test for equality between two points
  # other is a Point object
  # returns a Boolean
  def __eq__ (self, other):
    
      tol = 1.0e-06
      return ((abs(self.x - other.x) < tol) and (abs(self.y - other.y) < tol)
              and (abs(self.z - other.z) < tol))

class Sphere (object):
  # constructor with default values
  def __init__ (self, x = 0, y = 0, z = 0, radius = 1):
    
      self.center = Point(x,y,z) 
      self.radius = radius

  # returns string representation of a Sphere of the form:
  # Center: (x, y, z), Radius: value
  def __str__ (self):

      return ("Center: (" + str(self.center.x) + ", " + str(self.center.y)
              + ", " + str(self.center.z) + "), Radius: " + str(format(self.radius)))

  # compute surface area of Sphere
  # returns a floating point number
  def area (self):
    
      area = 4 * math.pi * self.radius ** 2
      return area

  # compute volume of a Sphere
  # returns a floating point number
  def volume (self):
    
      vol = 4/3 * math.pi * self.radius ** 3
      return vol

  # determines if a Point is strictly inside the Sphere
  # p is Point object
  # returns a Boolean
  def is_inside_point (self, p):
    
      return self.center.distance(p) < self.radius

  # determine if another Sphere is strictly inside this Sphere
  # other is a Sphere object
  # returns a Boolean
  def is_inside_sphere (self, other):
    
      distBetw = self.center.distance(other.center)
      return (distBetw + other.radius ) < self.radius

  # determine if a Cube is strictly inside this Sphere
  # determine if the eight corners of the Cube are inside 
  # the Sphere
  # a_cube is a Cube object
  # returns a Boolean
  def is_inside_cube (self, a_cube):

    if self.center.distance(a_cube.corner1) >= self.radius:
      return False
    if self.center.distance(a_cube.corner2) >= self.radius:
      return False
    if self.center.distance(a_cube.corner3) >= self.radius:
      return False
    if self.center.distance(a_cube.corner4) >= self.radius:
      return False
    if self.center.distance(a_cube.corner5) >= self.radius:
      return False
    if self.center.distance(a_cube.corner6) >= self.radius:
      return False
    if self.center.distance(a_cube.corner7) >= self.radius:
      return False
    if self.center.distance(a_cube.corner8) >= self.radius:
      return False
    return True

  # determine if a Cylinder is strictly inside this Sphere
  # a_cyl is a Cylinder object
  # returns a Boolean
  def is_inside_cyl (self, a_cyl):

    if self.center.distance(a_cyl.center) + a_cyl.height /2 >= self.radius:
      return False
    if self.center.distance(a_cyl.center) + a_cyl.radius >= self.radius:
      return False
    return True

  # determine if another Sphere intersects this Sphere
  # there is a non-zero volume of intersection
  # other is a Sphere object
  # returns a Boolean
  def does_intersect_sphere (self, other):
    
    distBetw = self.center.distance(other.center)
    return distBetw < other.radius + self.radius 

  # determine if a Cube intersects this Sphere
  # there is a non-zero volume of intersection
  # there is at least one corner of the Cube in 
  # the Sphere
  # a_cube is a Cube object
  # returns a Boolean
  def does_intersect_cube (self, a_cube):
    
    if self.center.distance(a_cube.corner1) >= self.radius:
      return False
    if self.center.distance(a_cube.corner2) >= self.radius:
      return False
    if self.center.distance(a_cube.corner3) >= self.radius:
      return False
    if self.center.distance(a_cube.corner4) >= self.radius:
      return False
    if self.center.distance(a_cube.corner5) >= self.radius:
      return False
    if self.center.distance(a_cube.corner6) >= self.radius:
      return False
    if self.center.distance(a_cube.corner7) >= self.radius:
      return False
    if self.center.distance(a_cube.corner8) >= self.radius:
      return False
    if is_inside_cube(a_cube) == True:
      return False
    return True

  # return the largest Cube object that is circumscribed
  # by this Sphere
  # all eight corners of the Cube are on the Sphere
  # returns a Cube object
  def circumscribe_cube (self):

    return Cube(self.center.x, self.center.y, self.center.z,
                self.radius * 2 / math.sqrt(3))

class Cube (object):
  # Cube is defined by its center (which is a Point object)
  # and side. The faces of the Cube are parallel to x-y, y-z,
  # and x-z planes.
  def __init__ (self, x = 0, y = 0, z = 0, side = 1):
    
    self.center = Point(x,y,z)
    self.side = side
    self.corner1 = Point(side/2 + x, side/2 + y, side/2 + z)
    self.corner2 = Point(side/2 + x, side/2 + y, -side/2 + z)
    self.corner3 = Point(-side/2 + x, side/2 + y, -side/2 + z)
    self.corner4 = Point(-side/2 + x, side/2 + y, side/2 + z)
    self.corner5 = Point(-side/2 + x, -side/2 + y, side/2 + z)
    self.corner6 = Point(-side/2 + x, -side/2 + y, -side/2 + z)
    self.corner7 = Point(side/2 + x, -side/2 + y, -side/2 + z)
    self.corner8 = Point(side/2 + x, -side/2 + y, side/2 + z)

  # string representation of a Cube of the form: 
  # Center: (x, y, z), Side: value
  def __str__ (self):
    
    return ("Center: (" + str(self.center.x) + ", " + str(self.center.y)
            + ", " + str(self.center.z) + "), Side: " + format(self.side, ".4f"))

  # compute the total surface area of Cube (all 6 sides)
  # returns a floating point number
  def area (self):

    area = self.side ** 2 * 6
    return area

  # compute volume of a Cube
  # returns a floating point number
  def volume (self):

    vol = self.side ** 3
    return vol

  # determines if a Point is strictly inside this Cube
  # p is a point object
  # returns a Boolean
  def is_inside_point (self, p):

    if self.corner1.distance(p) > self.side:
      return False
    if self.corner2.distance(p) > self.side:
      return False
    if self.corner3.distance(p) > self.side:
      return False
    if self.corner4.distance(p) > self.side:
      return False
    if self.corner5.distance(p) > self.side:
      return False
    if self.corner6.distance(p) > self.side:
      return False
    if self.corner7.distance(p) > self.side:
      return False
    if self.corner8.distance(p) > self.side:
      return False
    return True

  # determine if a Sphere is strictly inside this Cube or
  # a_sphere is a Sphere object
  # returns a Boolean
  def is_inside_sphere (self, a_sphere):

    diffDist = self.center.distance(a_sphere.center)
    return (diffDist + a_sphere.radius) < self.side

  # determine if another Cube is strictly inside this Cube
  # other is a Cube object
  # returns a Boolean
  def is_inside_cube (self, other):

    if self.corner1.distance(other.center) > self.side:
      return False
    if self.corner2.distance(other.center) > self.side:
      return False
    if self.corner3.distance(other.center) > self.side:
      return False
    if self.corner4.distance(other.center) > self.side:
      return False
    if self.corner5.distance(other.center) > self.side:
      return False
    if self.corner6.distance(other.center) > self.side:
      return False
    if self.corner7.distance(other.center) > self.side:
      return False
    if self.corner8.distance(other.center) > self.side:
      return False
    return True

  # determine if a Cylinder is strictly inside this Cube
  # a_cyl is a Cylinder object
  # returns a Boolean
  def is_inside_cylinder (self, a_cyl):

    if self.center.distance(a_cyl.center) + a_cyl.radius > self.side:
      return False
    if self.center.distance(a_cyl.center) + a_cyl.height > self.side:
      return False
    return True

  # determine if another Cube intersects this Cube
  # there is a non-zero volume of intersection
  # there is at least one vertex of the other Cube
  # in this Cube
  # other is a Cube object
  # returns a Boolean
  def does_intersect_cube (self, other):

    if self.center.distance(other.center) >(math.hypot(self.side, self.side) + math.hypot(other.side, other.side)):
      return False
    return True

  # determine the volume of intersection if this Cube 
  # intersects with another Cube
  # other is a Cube object
  # returns a floating point number
  def intersection_volume (self, other):
       
    x = abs(self.center.x - other.center.x)
    y = abs(self.center.y - other.center.y)
    z = abs(self.center.z - other.center.z)
    diffX = (self.side + other.side) - ( x + self.side / 2 + other.side / 2)
    diffY = (self.side + other.side) - ( y + self.side / 2 + other.side / 2)
    diffZ = (self.side + other.side) - ( z + self.side / 2 + other.side / 2)
    return diffX * diffY * diffZ
    
  # return the largest Sphere object that is inscribed
  # by this Cube
  # Sphere object is inside the Cube and the faces of the
  # Cube are tangential planes of the Sphere
  # returns a Sphere object
  def inscribe_sphere (self):

    return Sphere(self.center.x, self.center.y, self.center.z, self.side / 2)
    

class Cylinder (object):
  # Cylinder is defined by its center (which is a Point object),
  # radius and height. The main axis of the Cylinder is along the
  # z-axis and height is measured along this axis
  def __init__ (self, x = 0, y = 0, z = 0, radius = 1, height = 1):
    
    self.center = Point(x,y,z)
    self.radius = radius
    self.height = height

  # returns a string representation of a Cylinder of the form: 
  # Center: (x, y, z), Radius: value, Height: value
  def __str__ (self):

    return ("Center: (" + str(self.center.x) + ", " + str(self.center.y) + ", " + str(self.center.z) + "), Radius: " + str(self.radius) + ", Height: " + str(self.height))

  # compute surface area of Cylinder
  # returns a floating point number
  def area (self):

    area = 2 * math.pi * self.radius * self.height + \
           2 * math.pi * self.radius ** 2
    return area

  # compute volume of a Cylinder
  # returns a floating point number
  def volume (self):

    vol = math.pi * self.radius ** 2 * self.height
    return vol

  # determine if a Point is strictly inside this Cylinder
  # p is a Point object
  # returns a Boolean
  def is_inside_point (self, p):

    if self.center.distance(p) >= self.height / 2:
      return False
    if self.center.distance(p) >= self.radius:
      return False
    return True

  # determine if a Sphere is strictly inside this Cylinder
  # a_sphere is a Sphere object
  # returns a Boolean
  def is_inside_sphere (self, a_sphere):

    if a_sphere.radius + abs(a_sphere.center.z - self.center.z) > self.radius:
      return False
    if a_sphere.center.distance(self.center) + a_sphere.radius > self.height / 2:
      return False
    return True

  # determine if a Cube is strictly inside this Cylinder
  # determine if all eight corners of the Cube are in
  # the Cylinder
  # a_cube is a Cube object
  # returns a Boolean
  def is_inside_cube (self, a_cube):

    if a_cube.side / 2 + abs(self.center.x - a_cube.center.x) > self.radius:
      return False
    if a_cube.side / 2 + abs(self.center.y - a_cube.center.y) > self.radius:
      return False
    if a_cube.side / 2 + abs(self.center.z - a_cube.center.z) >= self.height / 2:
      return False
    return True

  # determine if another Cylinder is strictly inside this Cylinder
  # other is Cylinder object
  # returns a Boolean
  def is_inside_cylinder (self, other):

    if abs(self.center.x - other.center.x) >= self.radius:
      return False
    if abs(self.center.y - other.center.y) >= self.radius:
      return False
    if abs(self.center.z - other.center.z) >= self.height / 2:
      return False
    return True
    
  # determine if another Cylinder intersects this Cylinder
  # there is a non-zero volume of intersection
  # other is a Cylinder object
  # returns a Boolean
  def does_intersect_cylinder (self, other):

    if abs(self.center.x - other.center.x) > self.radius + other.radius:
      return False
    if abs(self.center.y - other.center.y) > self.radius + other.radius:
      return False
    if abs(self.center.z - other.center.z) > self.height / 2 + other.height / 2:
      return False
    return True

def main():

  file = open("geometry.in.txt", "r")
  fptr = open("geometry.out.txt", "w")
  
  data = file.read().split()
  
  # interpret the input passed in as a string named "inpt"
  #data = inpt.split()
  # read the coordinates of the first Point p
  # create a Point object
  p = Point(float(data[0]), float(data[1]), float(data[2]))
  # read the coordinates of the second Point q
  # create a Point object
  q = Point(float(data[3]), float(data[4]), float(data[5]))
  # read the coordinates of the center and radius of sphereA
  # create a Sphere object
  sphereA = Sphere(float(data[6]), float(data[7]), float(data[8]), float(data[9]))
  # read the coordinates of the center and radius of sphereB
  # create a Sphere object
  sphereB = Sphere(float(data[10]), float(data[11]), float(data[12]), float(data[13]))
  # read the coordinates of the center and side of cubeA
  # create a Cube object
  cubeA = Cube(float(data[14]), float(data[15]), float(data[16]), float(data[17]))
  # read the coordinates of the center and side of cubeB
  # create a Cube object
  cubeB = Cube(float(data[18]), float(data[19]), float(data[20]), float(data[21]))
  # read the coordinates of the center, radius and height of cylA
  # create a Cylinder object
  cylA = Cylinder(float(data[22]), float(data[23]), float(data[24]), float(data[25]), float(data[26]))
  # read the coordinates of the center, radius and height of cylB
  # create a Cylinder object
  cylB = Cylinder(float(data[27]), float(data[28]), float(data[29]), float(data[30]), float(data[31]))


  # create a Point object and write its coordinates with fptr
  fptr.write("Point p: ")
  fptr.write(str(p))
  fptr.write('\n')
  # read the coordinates of the second Point q
  # create a Point object and write its coordinates with fptr
  fptr.write("Point q: ")
  fptr.write(str(q))  
  fptr.write('\n')
  # read the coordinates of the center and radius of sphereA
  # create a Sphere object and write it with fptr
  fptr.write("sphereA: ")
  fptr.write(str(sphereA))
  fptr.write('\n')
  # read the coordinates of the center and radius of sphereB
  # create a Sphere object and write it with fptr
  fptr.write("sphereB: ")
  fptr.write(str(sphereB))
  fptr.write('\n')
  # read the coordinates of the center and side of cubeA
  # create a Cube object and write it with fptr
  fptr.write("cubeA: ")
  fptr.write(str(cubeA))
  fptr.write('\n')
  # read the coordinates of the center and side of cubeB
  # create a Cube object and write it with fptr
  fptr.write("cubeB: ")
  fptr.write(str(cubeB))
  fptr.write('\n')
  # read the coordinates of the center, radius and height of cylA
  # create a Cylinder object and write it with fptr
  fptr.write("cylA: ")
  fptr.write(str(cylA))
  fptr.write('\n')
  # read the coordinates of the center, radius and height of cylB
  # create a Cylinder object and write it with fptr
  fptr.write("cylB: ")
  fptr.write(str(cylB))
  fptr.write('\n')
  fptr.write('\n')

  distPQ = p.distance(q)
  fptr.write("Distance between p and q: ")
  fptr.write(format(distPQ, ".5f"))
  fptr.write('\n')

  # write with fptr area of sphereA
  fptr.write("Area of sphereA: ")
  fptr.write(format(sphereA.area(), ".5f"))
  fptr.write('\n')
  # write with fptr volume of sphereA
  fptr.write("Volume of sphereA: ")
  fptr.write(format(sphereA.volume(), ".5f"))
  fptr.write('\n')
  # write with fptr if Point p is inside sphereA
  booleanP = sphereA.is_inside_point(p)
  if booleanP  == True:
      fptr.write("Point p is inside sphereA")
  else:
      fptr.write("Point p is not inside sphereA")
  fptr.write('\n')
  # write with fptr if sphereB is inside sphereA
  booleanSB = sphereA.is_inside_sphere(sphereB)
  if booleanSB  == True:
      fptr.write("sphereB is inside sphereA")
  else:
      fptr.write("sphereB is not inside sphereA")
  fptr.write('\n')
  # write with fptr if cubeA is inside sphereA
  booleanCA = sphereA.is_inside_cube(cubeA)
  if booleanCA  == True:
      fptr.write("cubeA is inside sphereA")
  else:
      fptr.write("cubeA is not inside sphereA")
  fptr.write('\n')
  # write with fptr if cylA is inside sphereA
  booleanCYA = sphereA.is_inside_cyl(cylA)
  if booleanCYA  == True:
      fptr.write("cylA is inside sphereA")
  else:
      fptr.write("cylA is not inside sphereA")
  fptr.write('\n')
  # write with fptr if sphereA intersects with sphereB
  booleanSABinter = sphereA.does_intersect_sphere(sphereB)
  if booleanSABinter  == True:
      fptr.write("sphereA does intersect sphereB")
  else:
      fptr.write("sphereA does not intersect sphereB")
  fptr.write('\n')
  # write with fptr if cubeB intersects with sphereB
  booleanCBSBinter = sphereA.does_intersect_cube(cubeB)
  if booleanSABinter  == True:
      fptr.write("cubeB does intersect sphereB")
  else:
      fptr.write("cubeB does not intersect sphereB")
  fptr.write('\n')
  # write with fptr the largest Cube that is circumscribed by sphereA
  sphereNew = sphereA.circumscribe_cube()
  fptr.write("Largest Cube circumscribed by sphereA: ")
  fptr.write(str(sphereNew))
  fptr.write('\n')

  # write with fptr area of cubeA
  fptr.write("Area of cubeA: ")
  fptr.write(format(cubeA.area(), ".1f"))
  fptr.write('\n')
  # write with fptr volume of cubeA
  fptr.write("Volume of cubeA: ")
  fptr.write(format(cubeA.volume(), ".1f"))
  fptr.write('\n')
  # write with fptr if Point p is inside cubeA
  booleanP = cubeA.is_inside_point(p)
  if booleanP  == True:
      fptr.write("Point p is inside cubeA")
  else:
      fptr.write("Point p is not inside cubeA")
  fptr.write('\n')
  # write with fptr if sphereA is inside cubeA
  booleanSB = cubeA.is_inside_cube(sphereA)
  if booleanSB  == True:
      fptr.write("sphereA is inside cubeA")
  else:
      fptr.write("sphereA is not inside cubeA")
  fptr.write('\n')
  # write with fptr if cubeB is inside cubeA
  booleanCA = cubeA.is_inside_cube(cubeB)
  if booleanCA  == True:
      fptr.write("cubeB is inside cubeA")
  else:
      fptr.write("cubeB is not inside cubeA")
  fptr.write('\n')
  # write with fptr if cylA is inside cubeA
  booleanCYA = cubeA.is_inside_cylinder(cylA)
  if booleanCYA  == True:
      fptr.write("cylA is inside cubeA")
  else:
      fptr.write("cylA is not inside cubeA")
  fptr.write('\n')
  # write with fptr if cubeA intersects with cubeB
  booleanSABinter = cubeA.does_intersect_cube(cubeB)
  if booleanSABinter  == True:
      fptr.write("cubeA does intersect cubeB")
  else:
      fptr.write("cubeA does not intersect cubeB")
  fptr.write('\n')
  # write with fptr the intersection volume of cubeA and cubeB
  volInter = cubeA.intersection_volume(cubeB)
  fptr.write("Intersection volume of cubeA and cubeB: ")
  fptr.write(format(volInter, ".3f"))
  fptr.write('\n')
  # write with fptr the largest Sphere object inscribed by cubeA
  sphereNew = cubeA.inscribe_sphere()
  fptr.write("Largest Sphere inscribed by cubeA: ")
  fptr.write(str(sphereNew))
  fptr.write('\n')
    
  # write with fptr area of cylA
  fptr.write("Area of cylA: ")
  fptr.write(format(cylA.area(), ".5f"))
  fptr.write('\n')
  # write with fptr volume of cylA
  fptr.write("Volume of cylA: ")
  fptr.write(format(cylA.volume(), ".5f"))
  fptr.write('\n')
  # write with fptr if Point p is inside cylA
  booleanP = cylA.is_inside_point(p)
  if booleanP  == True:
      fptr.write("Point p is inside cylA")
  else:
      fptr.write("Point p is not inside cylA")
  fptr.write('\n')
  # write with fptr if sphereA is inside cylA
  booleanSB = cylA.is_inside_sphere(sphereA)
  if booleanSB  == True:
      fptr.write("sphereA is inside cylA")
  else:
      fptr.write("sphereA is not inside cylA")
  fptr.write('\n')
  # write with fptr if cubeA is inside cylA
  booleanCA = cylA.is_inside_cube(cubeA)
  if booleanCA  == True:
      fptr.write("cubeA is inside cylA")
  else:
      fptr.write("cubeA is not inside cylA")
  fptr.write('\n')
  # write with fptr if cylB is inside cylA
  booleanCYA = cylA.is_inside_cylinder(cylB)
  if booleanCYA  == True:
      fptr.write("cylB is inside cylA")
  else:
      fptr.write("cylB is not inside cylA")
  fptr.write('\n')
  # write with fptr if cylB intersects with cylA
  booleanSABinter = cylA.does_intersect_cylinder(cylB)
  if booleanSABinter  == True:
      fptr.write("cylA does intersect cylB")
  else:
      fptr.write("cylA does not intersect cylB")
  fptr.write('\n')

  file.close()
  fptr.close()

if __name__ == "__main__":
  main()
