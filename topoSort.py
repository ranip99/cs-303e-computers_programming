
class Stack (object):
    def __init__ (self):
        self.stack = []
    
    # add an item to the top of the stack
    def push (self, item):
        self.stack.append (item)

    # remove an item from the top of the stack
    def pop (self):
        return self.stack.pop()

    # check the item on the top of the stack
    def peek (self):
        return self.stack[-1]

    # check if the stack if empty
    def is_empty (self):
        return len (self.stack) == 0

    # return the number of elements in the stack
    def size (self):
        return len (self.stack)


class Queue (object):
    def __init__ (self):
        self.queue = []

    # add an item to the end of the queue
    def enqueue (self, item):
        self.queue.append (item)

    # remove an item from the beginning of the queue
    def dequeue (self):
        return self.queue.pop(0)

    # check if the queue is empty
    def is_empty (self):
        return len (self.queue) == 0

    # return the size of the queue
    def size (self):
        return len (self.queue)

    #check the first item of the queue
    def peek(self):
        return self.queue[0]

class Vertex (object):
    def __init__ (self, label):
        self.label = label
        self.visited = False

    # determine if a vertex was visited
    def was_visited (self):
        return self.visited

    # determine the label of the vertex
    def get_label (self):
        return self.label

    # string representation of the vertex
    def __str__ (self):
        return str (self.label)

class Graph (object):
    def __init__ (self):
        self.Vertices = []
        self.adjMat = []

    # check if a vertex is already in the graph
    def has_vertex (self, label):
        nVert = len (self.Vertices)
        for i in range (nVert):
            if (label == (self.Vertices[i]).get_label()):
                return True
        return False

    # given the label get the index of a vertex
    def get_index (self, label):
        nVert = len (self.Vertices)
        for i in range (nVert):
            if (label == (self.Vertices[i]).get_label()):
                return i
        return -1

    # add a Vertex with a given label to the graph
    def add_vertex (self, label):
        if (not self.has_vertex (label)):
            self.Vertices.append (Vertex (label))

        # add a new column in the adjacency matrix
        nVert = len (self.Vertices)
        for i in range (nVert - 1):
            (self.adjMat[i]).append (0)

        # add a new row for the new vertex
        new_row = []
        for i in range (nVert):
            new_row.append (0)
        self.adjMat.append (new_row)

    # add weighted directed edge to graph
    def add_directed_edge (self, start, finish, weight = 1):
        self.adjMat[start][finish] = weight

    # add weighted undirected edge to graph
    def add_undirected_edge (self, start, finish, weight = 1):
        self.adjMat[start][finish] = weight
        self.adjMat[finish][start] = weight

    # return an unvisited vertex adjacent to vertex v (index)
    def get_adj_unvisited_vertex (self, v):
        nVert = len (self.Vertices)
        for i in range (nVert):
            if (self.adjMat[v][i] > 0) and (not (self.Vertices[i]).was_visited()):
                return i
        return -1

    # do a depth first search in a graph
    def dfs (self, v):
        # create the Stack
        theStack = Stack ()
        x = []

        # mark the vertex v as visited and push it on the Stack
        (self.Vertices[v]).visited = True
        #print (self.Vertices[v])
        theStack.push (v)

        # visit all the other vertices according to depth
        while (not theStack.is_empty()):
            # get an adjacent unvisited vertex
            u = self.get_adj_unvisited_vertex (theStack.peek())
            if (u == -1):
                u = theStack.pop()
            else:
                
                (self.Vertices[u]).visited = True
                #print (self.Vertices[u])
                x.append(self.Vertices[u].label)
                theStack.push (u)
        
        # the stack is empty, let us rest the flags
        nVert = len (self.Vertices)
        for i in range (nVert):
            (self.Vertices[i]).visited = False
        return x

    # do the breadth first search in a graph
    def bfs (self, v):
        pass  

    # delete a vertex from the vertex list and all edges from and
    # to it in the adjacency matrix
    def delete_vertex (self, vertexLabel):
        nVert = len(self.Vertices)
        idx = self.get_index(vertexLabel)
        self.Vertices.remove(self.Vertices[idx])
        self.adjMat.pop(idx)
        for j in range(nVert - 1):
            del self.adjMat[j][idx]

    # determine if a directed graph has a cycle
    # this function should return a boolean and not print the result
    def has_cycle (self):

        for i in range(len(self.Vertices)):
            if self.Vertices[i].visited == False:
                if self.cyc(self.Vertices[i],-1) == True:
                    nVert = len (self.Vertices)
                    for i in range (nVert):
                        (self.Vertices[i]).visited = False
                    return True
        nVert = len (self.Vertices)
        for i in range (nVert):
            (self.Vertices[i]).visited = False
        return False

    def cyc(self, name, x):
        
        idx = self.get_index(name)
        self.Vertices[idx].visited = True
        if (self.dfs(idx)) == []:
            return False
        if len(self.dfs(idx)) < len(self.Vertices) - 1:
            return False
        return True
        '''for item in self.Vertices:
            idx = self.get_index(item)
            if self.Vertices[idx].visited == False:
                if self.cyc(item, name):
                    return True
            elif x != item:
                return True
        return False'''
        
    # return a list of vertices after a topological sort
    # this function should not print the list
    def toposort (self):

        top = []
        while self.Vertices != []:
            inDegrees = []
            names = []
            temp = 0

            for i in range(len(self.Vertices)):
                for j in range(len(self.Vertices)):
                    temp += self.adjMat[j][i]
                inDegrees.append(temp)
                names.append(self.Vertices[i].label)
                temp = 0
        
            count = 0
            for i in inDegrees:
                if i == 0:
                    top.append(names[count])
                    self.delete_vertex(names[count])
                    inDegrees.pop(count)
                    names.pop(count)
                    count -= 1
                else:
                    count += 1

        return top


def main():
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    
    # create a Graph object (add vertices and edges)
    theGraph = Graph()
    
    # Get num vertices
    num_vertices = int(input().strip())
    for vert in range(num_vertices):
        theGraph.add_vertex(input().strip())
     
    # Get num edges
    num_edges = int(input().strip())
    for edge in range(num_edges):
        edge_to_edge = input().strip()
        start = theGraph.get_index(edge_to_edge[0])
        finish = theGraph.get_index(edge_to_edge[-1])
        theGraph.add_directed_edge(start, finish)

    # test if a directed graph has a cycle
    if (theGraph.has_cycle()):
        fptr.write("The Graph has a cycle.")
    else:
        fptr.write("The Graph does not have a cycle.")

    # test topological sort
    if (not theGraph.has_cycle()):
        vertex_list = theGraph.toposort()
        fptr.write("\nList of vertices after toposort\n")
        for i in range(len(vertex_list)): 
            fptr.write(str(vertex_list[i]))
            if i < len(vertex_list) - 1:
                fptr.write('\n')
    
    fptr.close()
        
if __name__ == '__main__':
    main()
