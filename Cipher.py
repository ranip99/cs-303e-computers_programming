
from math import ceil, sqrt

#temp is a 2D array/list
def print_arr (temp):
    mx = max((len(str(ele)) for sub in temp for ele in sub))
    for row in temp:
        print(" ".join(["{:<{mx}}".format(ele,mx=mx) for ele in row]))
        
# Input: strng is a string of 100 or less of upper case, lower case, 
#        and digits
# Output: function returns an encrypted string 
def encrypt ( strng ):

    l = len(strng)
    m = ceil(sqrt(l)) ** 2
    k = int(sqrt(m))
    asterisks = m - l

    #add any necessary asterisks
    for i in range(asterisks):
        strng = strng + "*"

    #make two lists k x k with 0's
    strList = []
    newList = []
    for i in range(k):
        row = []
        col = []
        for y in range(k):
            row.append(0)
            col.append(0)
        strList.append(col)
        newList.append(row)

    #fill the list with the string
    count = 0
    for i in range(k):
        for j in range(k):
            strList[i][j] = strng[count]
            count+= 1

    #rotate the list 90 degrees
    x = k - 1
    for j in range(k):
        for i in range(k):
            newList[i][x-j] = strList[j][i]

    #make the list back into a string removing *
    newString = ""
    for j in range(len(newList)):
        for i in range(len(newList)):
            if newList[j][i] == "*":
                continue
            newString += newList[j][i]

    return newString

# Input: strng is a string of 100 or less of upper case, lower case, 
#        and digits
# Output: function returns an encrypted string 
def decrypt ( strng ):

    l = len(strng)
    m = ceil(sqrt(l)) ** 2
    k = int(sqrt(m))
    asterisks = m - l

    #make three lists k x k with 0's
    strList = []
    newList = []
    for i in range(k):
        row = []
        col = []
        x = []
        for y in range(k):
            row.append(0)
            col.append(0)
        strList.append(col)
        newList.append(row)

    ast = ""
    zeros = ""
    #add any necessary asterisks
    for i in range(asterisks):
        ast = ast + "*"
    for i in range(m-asterisks):
        zeros += "0"
    fill = zeros + ast
                  
    #fill the list with the * and 0's
    count = 0
    for i in range(k):
        for j in range(k):
            strList[i][j] = fill[count]
            count+= 1

    #turn the list to have asterisks in right place 
    x = k - 1
    for j in range(k):
        for i in range(k):
            newList[i][x-j] = strList[j][i]

    #fill the list with the string
    count = 0
    for i in range(k):
        for j in range(k):
            if newList[i][j] ==  "*":
                continue
            else:
                newList[i][j] = strng[count]
                count+= 1
     
    #rotate the list 90 degrees
    x = k-1
    for j in range(k):
        for i in range(k):
            strList[j][i] = newList[i][x-j]
            
    #make the list back into a string removing *
    newStr = ""
    for j in range(k):
        for i in range(k):
            if strList[j][i] == "*":
                continue
            newStr += strList[j][i]
            
    return newStr

def main():
  # open file cipher.in and read the two strings P and Q
    inFile = open("cipher.in.txt", "r")
    p = inFile.readline().strip()
    q = inFile.readline().strip()

    #if len(strng) > 100 or len(strng) < 1:
        #return ("Not Valid Length")

  # encrypt the string P
    pEncrypted = encrypt(p)

  # decrypt the string Q
    qDecrypted = decrypt(q)

  # open file cipher.out and write the encrypted string of P
    outFile = open("cipher.out.txt", "w")
    outFile.write(pEncrypted + "\n")
    
  # and the decrypted string of Q
    outFile.write(qDecrypted)

if __name__ == "__main__":
    main()
