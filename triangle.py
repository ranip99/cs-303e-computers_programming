
import time

# Input: grid a 2-D list of integers
# Output: returns a single integer that is the greatest path sum
#         using exhaustive search
def exhaustive_search (grid, idx, row, total, lst):
    
    if row >= len(grid) or idx >= len(grid):
        lst.append(total)
        return lst
    else:
        return exhaustive_search(grid, idx, row  + 1, total + grid[row][idx], lst) \
               and exhaustive_search(grid, idx + 1, row + 1, total + grid[row][idx], lst)

# Input: grid a 2-D list of integers
# Output: returns a single integer that is the greatest path sum
#         using the greedy approach
def greedy (grid):

    idx = 0
    row = 0
    total = 0
    while row < len(grid):
        total += max(grid[row][idx], grid[row][idx + 1])
        if grid[row][idx] >= grid[row][idx + 1]:
            idx = idx
        else:
            idx += 1
        row += 1
        
    return total

# Input: grid a 2-D list of integers
# Output: returns a single integer that is the greatest path sum
#         using divide and conquer (recursive) approach
def rec_search (grid, idx, row, total):
    
    if row >= len(grid) or idx >= len(grid):
        return total
    else:
        return max(rec_search(grid, idx, row  + 1, total + grid[row][idx]),
                   rec_search(grid, idx + 1, row + 1, total + grid[row][idx]))

# Input: grid a 2-D list of integers
# Output: returns a single integer that is the greatest path sum
#         using dynamic programming
def dynamic_prog (grid):
    
    for i in range(len(grid)-2,-1,-1):
        for j in range(len(grid)-1):
            if grid[i+1][j] > grid[i+1][j+1]:
                grid[i][j] += grid[i+1][j]
            else:
                grid[i][j] += grid[i+1][j+1]
    return grid[0][0]

# reads the file and returns a 2-D list that represents the triangle
# Input: none
# Output: returns a 2-D list that represents the triangle
def read_file ():
    pass

# Input: no input
# Output: a string denoting all test cases have passed
def test_cases ():
  # write your own test cases

  return "all test cases passed"

def main ():
  # read triangular grid from file
  '''inFile = open("triangle.in.txt", "r")
  rows = int(inFile.readline())
  grid = []
  for i in range(rows):
    r = []
    for j in range(rows):
      x = inFile.readline().split()
      r.append(int(x[j]))
    grid.append(r)
  print(grid)'''

  grid = [[33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [6, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [58, 66, 61, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [42, 62, 86, 75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [19, 4, 7, 7, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [2, 15, 57, 13, 68, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0], [50, 12, 85, 46, 7, 53, 77, 0, 0, 0, 0, 0, 0, 0, 0], [84, 88, 22, 35, 31, 67, 55, 3, 0, 0, 0, 0, 0, 0, 0], [36, 86, 48, 95, 40, 99, 3, 70, 25, 0, 0, 0, 0, 0, 0], [21, 72, 61, 37, 36, 10, 7, 43, 20, 86, 0, 0, 0, 0, 0], [38, 76, 23, 51, 86, 9, 10, 88, 99, 38, 3, 0, 0, 0, 0], [18, 38, 37, 47, 17, 90, 30, 83, 58, 69, 33, 90, 0, 0, 0], [34, 57, 5, 62, 44, 27, 25, 89, 80, 40, 34, 1, 24, 0, 0], [49, 95, 33, 56, 83, 8, 16, 24, 16, 22, 16, 80, 35, 67, 0], [67, 48, 30, 4, 67, 52, 53, 48, 96, 13, 1, 7, 80, 11, 34]]

  ti = time.time()
  tot1 = max(exhaustive_search(grid, 0, 0, 0, []))
  # output greates path from exhaustive search
  tf = time.time()
  del_t = tf - ti
  # print time taken using exhaustive search
  print("ex:", tot1, del_t)

  ti = time.time()
  tot2 = greedy(grid)
  # output greates path from greedy approach
  tf = time.time()
  del_t = tf - ti
  # print time taken using greedy approach
  print("greedy:", tot2, del_t)

  ti = time.time()
  tot3 = rec_search(grid, 0, 0, 0)
  # output greates path from divide-and-conquer approach
  tf = time.time()
  del_t = tf - ti
  # print time taken using divide-and-conquer approach
  print("rec:", tot3, del_t)

  ti = time.time()
  tot4 = dynamic_prog(grid)
  # output greates path from dynamic programming 
  tf = time.time()
  del_t = tf - ti
  # print time taken using dynamic programming
  print("dynamic:", tot4, del_t)

if __name__ == "__main__":
  main()
