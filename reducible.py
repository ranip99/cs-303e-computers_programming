#  File: reducible.py

#  Description: longest reducible words

#  Student Name: Rani Patel

#  Student UT EID: rjp2524

#  Course Name: CS 313E

#  Unique Number: 84825

#  Date Created: 7/18/2020

#  Date Last Modified: 7/22/2020

# Input: takes as input a positive integer n
# Output: returns True if n is prime and False otherwise
def is_prime ( n ):
    
    if (n == 1):
        return False

    limit = int (n ** 0.5) + 1
    div = 2
    while (div < limit):
        if (n % div == 0):
          return False
        div += 1
    return True

# Input: takes as input a string in lower case and the size
#        of the hash table 
# Output: returns the index the string will hash into
def hash_word (s, size):
    
    hash_idx = 0
    for j in range (len(s)):
        letter = ord (s[j]) - 96
        hash_idx = (hash_idx * 26 + letter) % size
    return hash_idx

# Input: takes as input a string in lower case and the constant
#        for double hashing 
# Output: returns the step size for that string 
def step_size (s, const):
    
    num = hash_word(s, const)
    return const - (num % const)

# Input: takes as input a string and a hash table 
# Output: no output; the function enters the string in the hash table, 
#         it resolves collisions by double hashing
def insert_word (s, hash_table):
    
    idx1 = hash_word(s, len(hash_table))
    step = step_size(s, 13)
    
    if hash_table[idx1] == "":
       hash_table[idx1] = s
       
    else:
        idx2 = idx1
        while hash_table[idx2] != "":
            idx2 = (idx2 + step) % len(hash_table)
        hash_table[idx2] = s

            
# Input: takes as input a string and a hash table 
# Output: returns True if the string is in the hash table 
#         and False otherwise
def find_word (s, hash_table):

    '''if s in hash_table:
        return True
    else:
        return False'''

    idx1 = hash_word(s, len(hash_table))
    
    if hash_table[idx1] == s:
        return True
    else:
        step = step_size(s, 13)
        idx2 = idx1
        count = 0
        while hash_table[idx2] != s:
            if count >= len(hash_table):
                return False
            count += 1
            idx2 = (idx2 + step) % len(hash_table)
                    

# Input: string s, a hash table, and a hash_memo 
#        recursively finds if the string is reducible
# Output: if the string is reducible it enters it into the hash memo 
#         and returns True and False otherwise
def is_reducible (s, hash_table, hash_memo):

    if len(s) == 1:
        if s == "a" or s == "i" or s == "o":
            return True
        return False
    else:
        for i in range(len(s)):
            temp = s[:i] + s[i+1:]
            if len(temp) < 2:
                return is_reducible(temp, hash_table, hash_memo)
            if find_word(temp, hash_table):
                return is_reducible(temp, hash_table, hash_memo)
        return False


# Input: string_list a list of words
# Output: returns a list of words that have the maximum length
def get_longest_words (string_list):

    length = 0
    words = []
    for word in string_list:
        if len(word) > length:
            length = len(word)
    for word in string_list:
        if len(word) == length:
            words.append(word)
    return words

def main():
    # create an empty word_list
    word_list = []

    # open the file words.txt
    inFile = open("words.txt", "r")

    # read words from words.txt and append to word_list
    line = inFile.readline().strip()
    word_list.append(line)
    while line != "":
        line = inFile.readline().strip()
        word_list.append(line)
    
    # close file words.txt
    inFile.close()

    # find length of word_list
    length = len(word_list)

    # determine prime number N that is greater than twice
    # the length of the word_list
    new = 2 * length + 1
    prime = is_prime(new)
    count = 0
    while prime != True:
        count += 2
        prime = is_prime(new + count)
    new += count
    
    # create an empty hash_list
    hash_list = []

    # populate the hash_list with N blank strings
    for i in range(new):
        hash_list.append("")

    # hash each word in word_list into hash_list
    # for collisions use double hashing
    for i in range(len(word_list)):
        insert_word(word_list[i], hash_list)

    # create an empty hash_memo of size M
    # we do not know a priori how many words will be reducible
    # let us assume it is 10 percent (fairly safe) of the words
    # then M is a prime number that is slightly greater than 
    # 0.2 * size of word_list
    m = int(0.2 * len(word_list)) + 1
    while is_prime(m) != True:
        m += 1
    hash_memo = []

    # populate the hash_memo with M blank strings
    for i in range(m):
        hash_memo.append("")

    # create an empty list reducible_words
    reducible_words = []
    
    '''print(is_reducible('complecting', hash_list, hash_memo))'''
    
    # for each word in the word_list recursively determine
    # if it is reducible, if it is, add it to reducible_words
    for word in word_list:
        if is_reducible(word, hash_list, hash_memo):
            reducible_words.append(word)
                
    # find words of the maximum length in reducible_words
    longest = get_longest_words(reducible_words)
    
    # print the words of maximum length in alphabetical order
    # one word per line
    longest.sort()
    for word in longest:
        print(word)

# This line above main is for grading purposes. It will not 
# affect how your code will run while you develop and test it.
# DO NOT REMOVE THE LINE ABOVE MAIN
if __name__ == "__main__":
  main()
