#  File: DayOfTheWeek.py
#  Description: Output day of the week given specific date.
#  Student's Name: Rani Patel
#  Student's UT EID: rjp2524
#  Course Name: CS 303E 
#  Unique Number: 50191
#
#  Date Created: 02/18/2020
#  Date Last Modified: 02/19/2020

def main():

    #Receive year, month, and day
    year = int(input("Please enter the year (an integer): "))
    month = input("Please enter the month (a string): ")
    day = int(input("Please enter the day (an integer): "))

    #for January and February subtract 1 from the year
    #start with a = 12 for January
    if month == "January":
        a = 11
        year = year - 1

    elif month == "February":
        a = 12
        year = year - 1

    elif month == "March":
        a = 1
        
    elif month == "April":
        a = 2

    elif month == "May":
        a = 3

    elif month == "June":
        a = 4

    elif month == "July":
        a = 5

    elif month == "August":
        a = 6
        
    elif month == "September":
        a = 7
        
    elif month == "October":
        a = 8
        
    elif month == "November":
        a = 9

    elif month == "December":
        a = 10

        
    #use calculation algorithm
    d = year // 100
    c = year % 100
    b = day
    w = (13 * a - 1 ) // 5
    x = c // 4
    y = d // 4
    z = w + x + y + b + c - 2 * d
    r = z % 7
    r = (r + 7) % 7

    #print statement without new line at the end
    print("The day of the week is", end = " ")

    #print day depending on value of r
    if r == 0:
        print("Sunday.")

    elif r == 1:
        print("Monday.")

    elif r == 2:
        print("Tuesday.")

    elif r == 3:
        print("Wednesday.")

    elif r == 4:
        print("Thursday.")

    elif r == 5:
        print("Friday.")

    elif r == 6:
        print("Saturday.")


main()
